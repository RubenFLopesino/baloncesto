import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


public class PruebasPhantomjsIT {
    private static WebDriver driver=null;

    @Test
    public void tituloIndexTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
        System.out.println(driver.getTitle());
        driver.close();
        driver.quit();
    }

    @Test
    void resetVotes()
    {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");

        driver.findElement(By.id("resetearVotos")).click();

        driver.findElement(By.id("verResultados")).click();

        WebElement listaVotacion = driver.findElement(By.id("listaVotacion"));
        ArrayList<WebElement> voteElements = new ArrayList<>(listaVotacion.findElements(By.className("votosJugador")));
        boolean expectedResult = true;
        AtomicBoolean verify = new AtomicBoolean(true);
        voteElements.stream().forEach((element) -> {
            System.out.println(element.getText());
            if (!element.getText().trim().equals("0")) {
                verify.set(false);
            }
        });

        assertEquals(expectedResult, verify.get(),
                "No se ha realizado la puesta a 0 de los votos.");
    }

    @Test
    void voteOtherPlayer()
    {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new
        String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");

        //Se marca el radio button para votar por otro jugador
        driver.findElement(By.id("radioOtroJugador")).click();
        //Se introduce en su campo de texto el nombre de un jugador
        String nombreNuevaVotacion = "Luka Doncic";
        driver.findElement(By.id("textInputOtroJugador")).sendKeys(nombreNuevaVotacion);
        //Se realiza el envío del voto realizado
        driver.findElement(By.id("mandarVoto")).click();
        //Se vuelve a la página principal
        driver.findElement(By.id("volverHome")).click();

        //Se abre la página para visualizar los votos
        driver.findElement(By.id("verResultados")).click();

        boolean expectedResult = true;
        AtomicBoolean verify = new AtomicBoolean(false);

        WebElement listaVotacion = driver.findElement(By.id("listaVotacion"));
        ArrayList<WebElement> elementosLista = new ArrayList<>(listaVotacion.findElements(By.tagName("li")));
        
        elementosLista.stream().forEach(elemento -> {
            String nombre = elemento.findElement(By.className("nombreJugador")).getText().trim();
            String votos = elemento.findElement(By.className("votosJugador")).getText().trim();
            System.out.println(nombreNuevaVotacion + votos);
            if (nombre.equals(nombreNuevaVotacion) && votos.equals("1")) {
                verify.set(true);
            }
        });


        assertEquals(expectedResult, verify.get(),
        "El jugador no ha recibido el voto.");
    }


}
