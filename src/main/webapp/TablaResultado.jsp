<%@ page import="java.util.*" %>
<html>
  <head>
    <title>Votaci&oacute;n mejor jugador liga ACB</title>
    <link rel="stylesheet" href="./resources/styles/style.css" />
  </head>
  <body class="resultado">
      <h1>Resultados de la votación</h1>
      <hr />
      <% 
      String jugadores = (String) session.getAttribute("jugadores");
      List<String> jugadoresList = Arrays.asList(jugadores.split("\\s*>\\s*"));
       %>
        <ul id="listaVotacion">
          <%
             for (int i = 0; i < jugadoresList.size(); i++) {
               List<String> datos = Arrays.asList(jugadoresList.get(i).split("\\s*-\\s*"));
                String nombre = datos.get(0);
                String votos = datos.get(1);
             
          %>
            <li id="<%=nombre%>">
              <span class="nombreJugador"><%=nombre%></span><br>
              <span class="votosJugador"><%=votos%></span>
            </li>
          <%}%>
        </ul>
    <br />
    <br />
    <a href="index.html"> Ir al comienzo</a>
  </body>
</html>
